﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    /// <summary>
    /// The class which represents a Business.
    /// </summary>
    [Serializable]
    public class Business
    {
        /// <summary>
        /// Initalizes and instance of the Business Class.
        /// </summary>
        public Business()
        {
            this.Employees = new List<Employee>();
            this.Jobs = new List<Job>();
        }

        /// <summary>
        /// Gets or sets the lsit of Employees.
        /// </summary>
        public List<Employee> Employees { get; set; }

        /// <summary>
        /// Gets or sets the list of Jobs.
        /// </summary>
        public List<Job> Jobs { get; set; }

        /// <summary>
        /// Add the Employee to the list of Employees.
        /// </summary>
        /// <param name="employee">The Employee to add to the list.</param>
        public void AddEmployee(Employee employee)
        {
            // Add employee to the employee list.
            this.Employees.Add(employee);
        }

        /// <summary>
        /// Add the Job to the list of Jobs.
        /// </summary>
        /// <param name="job">The Job to add to the list.</param>
        public void AddJob(Job job)
        {
            // Add job to the job list
            this.Jobs.Add(job);
        }

        /// <summary>
        /// The Business's jobs.
        /// </summary>
        public void DoWork()
        {
            // For each job, if job isn't completed, check each employees to see if they have hours and have them DoWork(). If job is completed after work, break from loop.
            foreach (Job job in this.Jobs)
            {
               if (job.JobCompleted == false)
                {
                    foreach (Employee employee in this.Employees)
                    {
                        employee.DoWork(job);

                        if (job.JobCompleted)
                        {
                            break;
                        }
                    }
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Test_Driven_Development
{
    /// <summary>
    /// The main class for the console app.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The main method for the console app.
        /// </summary>
        /// <param name="args">The arguments being passed in.</param>
#pragma warning disable IDE0060 // Remove unused parameter
        public static void Main(string[] args)
#pragma warning restore IDE0060 // Remove unused parameter
        {
            Business business = new Business();
            business.AddEmployee(new Employee(10, 10));
            business.AddEmployee(new Employee(10, 10));
            business.AddJob(new Job(15));
            business.DoWork();

            SerializeBusiness(business);

            Console.ReadLine();
        }

        /// <summary>
        /// Serialize the Business object.
        /// </summary>
        /// <param name="business">The object to serialize.</param>
        public static void SerializeBusiness(Business business)
        {
            // Create a new XmlSerializer instance with the type of the test class
            XmlSerializer serializerObj = new XmlSerializer(typeof(Business));

            // Create a new file stream to write the serialized object to a file
            TextWriter writeFileStream = new StreamWriter(@"C:\ADN\Business.xml");
            serializerObj.Serialize(writeFileStream, business);

            // Cleanup
            writeFileStream.Close();
        }
    }
}

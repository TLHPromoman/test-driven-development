﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    /// <summary>
    /// The class which represents a Job.
    /// </summary>
    [Serializable]
    public class Job
    {
        /// <summary>
        /// Initalizes and instance of the Job Class.
        /// </summary>
        /// <param name="timeInvestment">The job's time investment/cost.</param>
        public Job(int timeInvestment)
        {
            this.TimeInvestmentRemaining = timeInvestment;
        }

        /// <summary>
        /// Private constructor Job classe, with no parameters and an empty body (for Serialization).
        /// </summary>
        private Job()
        {
        }

        /// <summary>
        /// Gets or sets the job's cost.
        /// </summary>
        public decimal JobCost { get; set; }

        /// <summary>
        /// Gets or sets the job's remaining time investment.
        /// </summary>
        public int TimeInvestmentRemaining { get; set; }

        /// <summary>
        /// Gets a value indicating whether the Job was completed.
        /// </summary>
        public bool JobCompleted => this.TimeInvestmentRemaining == 0;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    /// <summary>
    /// The class which represents an Employee.
    /// </summary>
    [Serializable]
    public class Employee
    {
        /// <summary>
        /// The Hourly Wage.
        /// </summary>
        private decimal hourlyWage;

        /// <summary>
        /// The Scheduled Hours.
        /// </summary>
        private int hoursScheduled;

        /// <summary>
        /// Initalizes and instance of the Employee Class.
        /// </summary>
        /// <param name="hourlyWage">Employee's Hourly Wage.</param>
        /// <param name="hoursScheduled">Employee's Scheduled Hours.</param>
        public Employee(decimal hourlyWage, int hoursScheduled)
        {
            this.hourlyWage = hourlyWage;
            this.hoursScheduled = hoursScheduled;
        }

        /// <summary>
        /// Private constructor Employee classe, with no parameters and an empty body (for Serialization).
        /// </summary>
        private Employee()
        {
        }

        /// <summary>
        /// Gets or sets the Employee's Paycheck.
        /// </summary>
        public decimal Paycheck { get; set; }

        /// <summary>
        /// Employee's Work method.
        /// </summary>
        /// <param name="work">The job of work.</param>
        public void DoWork(Job work)
        {
            // If the hoursScheduled are less than the time remaining on the job, reduce the hours remaining on the job by the hours scheduled, and set hours scheduled to zero.
            // Else reduce hours scheduled by time remaining, and set time investment for job to 0.
            decimal payment;

            if (this.hoursScheduled < work.TimeInvestmentRemaining)
            {
                work.TimeInvestmentRemaining -= this.hoursScheduled;

                payment = this.hourlyWage * this.hoursScheduled;

                this.hoursScheduled = 0;
            }
            else
            {
                payment = this.hourlyWage * work.TimeInvestmentRemaining;

                this.hoursScheduled -= work.TimeInvestmentRemaining;

                work.TimeInvestmentRemaining = 0;
            }

            work.JobCost += 1.5m * payment;

            this.Paycheck += payment;
        }
    }
}
